import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraphModule, LoginComponent, StatbydayModule } from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { DisplayDataComponent } from './pages/display-data/display-data.component';
import { DxDataGridModule, DxFormModule, DxContextMenuModule, DxPopupModule, DxLoadPanelModule, DxButtonModule,
  DxTextBoxModule, DxBoxModule, DxSelectBoxModule, DxCheckBoxModule, DxResponsiveBoxModule, DxFileUploaderModule,
  DxGalleryModule, DxTabPanelModule, DxTreeListModule, DxTextAreaModule, DxLoadIndicatorModule } from 'devextreme-angular';
import { TestComponent } from './pages/test/test.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BoardsComponent } from './pages/boards/boards.component';
import { RmaComponent } from './pages/rma/rma.component';
import { BoardDetailComponent } from './pages/board-detail/board-detail.component';
import { DxTabsModule } from 'devextreme-angular';
import { DxTreeViewModule } from 'devextreme-angular/ui/tree-view';
import { ModelComponent } from './pages/model/model.component';
import { TestdefComponent } from './pages/testdef/testdef.component';
import { BoardtestComponent } from './pages/boardtest/boardtest.component';

const routes: Routes = [
  {
    path: 'btest',
    component: BoardtestComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'testdef',
    component: TestdefComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'model',
    component: ModelComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'rma',
    component: RmaComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'board-detail/:b_id',
    component: BoardDetailComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'board',
    component: BoardsComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'test',
    component: TestComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'display-data',
    component: DisplayDataComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: '**',
    redirectTo: 'home',
    canActivate: [ AuthGuardService ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    DxDataGridModule,
    DxFormModule,
    DxContextMenuModule,
    FontAwesomeModule,
    CommonModule,
    DxPopupModule,
    DxLoadPanelModule,
    DxButtonModule,
    DxTextBoxModule,
    DxSelectBoxModule,
    DxCheckBoxModule,
    DxResponsiveBoxModule,
    DxBoxModule,
    DxFileUploaderModule,
    DxGalleryModule,
    DxTabPanelModule,
    DxTabsModule,
    DxTreeViewModule,
    DxTreeListModule,
    DxTextAreaModule,
    DxLoadIndicatorModule,
    DxLoadPanelModule,
    FormsModule,
    ReactiveFormsModule,
    GraphModule,
    StatbydayModule    
  ],
  providers: [AuthGuardService],
  exports: [RouterModule],
  declarations: [
    HomeComponent, 
    ProfileComponent, 
    DisplayDataComponent, 
    TestComponent, 
    BoardsComponent, 
    RmaComponent, 
    BoardDetailComponent, 
    ModelComponent, 
    TestdefComponent,
    BoardtestComponent
   ]
})
export class AppRoutingModule { }
