import { Timestamp } from 'rxjs/internal/operators/timestamp';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

export interface heartbeat {
  hostname : string;
  last_ts : any;
  f_timeout: number;
}

export interface Station {
  hostname: string;
  state: string;
  st_timeout: number;
  board_uuid: string;
  testid_ctl: number;
  heartbeat: heartbeat;
}

export class StationStatus {
  status : string;
  stations : Station;
  constructor(status : string, station : Station){}
}

export interface StationMessage {
  status : string;
  stations : Station[];
}

export interface StHostVar {
  key : string,
  value : string
}

export interface StationHostVar {
  status : string;
  vars : StHostVar[];
}

export const Station_Results = ['T_OK', 'T_FAILED', 'T_RUNNING'];

export interface t_StationInfo {
  StationIcon : any;
  station : Station;
  StationStatus : string;
  StationResult : string;
  StationError : string;
}

export class StationInfo implements t_StationInfo {

  public  StationIcon : any;
  public  station : Station;
  public  StationStatus : string;
  public  StationResult : string;
  public  StationError : string;

  constructor(public StationStatusIcon : any, public st : Station){
    this.StationIcon = StationStatusIcon;
    this.station = st;
    this.updateState ();
    this.updateResult ();
  }

  updateResult(){
    let state = this.station.state;
    if(state == "FINISHED"){
      this.StationResult = "OK";
    }
    else if(state == 'STATION_ERROR' || state == 'TESTS_FAILED' ){
      this.StationResult = "FAILED";
    }
    else{
      this.StationResult = " ... ";
    }
  }

  updateState(){
    let state = this.station.state;
    if(state == "UNDEFINED"){
      this.StationStatus = "Station Reboot or Power OFF";
    }
    else if (state == "STATION_ERROR"){
      this.StationStatus = "Station Error";
    }
    else if (state == "FTP_KERNEL" || state == "FTP_DTB" || state == "MOUNT_NFS" || state == "KERNEL_BOOT"){
      this.StationStatus = "Board Boot process";
    }
    else if (state == "WAIT_TEST_START"){
      this.StationStatus = "WAIT Test Start";
    }
    else if (state == "TESTS_RUNNING"){
      this.StationStatus = "TEST in Progress";
    }
    else if (state == "TEST_OK"){
      this.StationStatus = "TEST Finish OK";
    }
    else if (state == "TESTS_FAILED"){
      this.StationStatus = "TEST Finish Failed";
    }
    else if(state == "TESTS_CHECKING_ENV"){
      this.StationStatus =  "Check Enviroment State";
    }
    else if(state == "EXTRATASKS_RUNNING"){
      this.StationStatus =  "Flash Firmware in Progress";
    }
    else if(state == "WAITING_FOR_SCANNER"){
      this.StationStatus =  "Read SCANNER ID";
    }
    else if(state == "FINISHED"){
      this.StationStatus =  "Board Test & Flash Finished";
    }
    else if(state == "STATION_REBOOT"){
      this.StationStatus =  "Station in Reboot Process";
    }
  }
}

