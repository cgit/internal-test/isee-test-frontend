import { ObjectUnsubscribedError } from 'rxjs';

export interface tm_Create_RMA {
  status : String,
  rid: Number
}

export interface I_RMA_Resolution {
  rid : Number,
  rma_status: string,
  resolution_date: Date
}

export interface I_RMA {
  rid: Number,
  rma_id: string,
  factory_code : string,
  customer: string,
  assigned_to: string,
  rma_report_time: Date,
  board_uuid : string,
  replaceboard: boolean,
  rma_resolution : I_RMA_Resolution
}

export interface tm_RMA_List {
  status : string,
  rma : I_RMA[]
}

export interface I_RMA_ACTION {
  aid : Number,
  action: string,
  text_data : string,
  file_gid: Number,
  rid: Number,
  ts: Date
}

export interface tm_RMA_Action {
  status : string,
  rma : I_RMA_ACTION[]
}

export interface tm_RMA_CreateAction {
  status: string,
  aid : Number
}

export interface tm_RMA_DeleteAction {
  status: string,
  result : Number
}


export interface tm_ACTION_list {
  status : string,
}

export interface tm_RMA_Action_File {
  status: string,
  file_gid : Number
}

export class T_RMA implements I_RMA {
  public board_uuid : string;
  public replaceboard: boolean;
  constructor(
    public rid: Number,
    public rma_id: string,
    public factory_code : string,
    public customer: string,
    public assigned_to: string,
    public rma_report_time: Date,
    public rma_resolution: I_RMA_Resolution
  ) {}
}

export class T_CREATE_RMA {
  constructor(
    public rma_id: string,
    public factory_code : string,
    public customer: string,
    public assigned_to: string,
    public rma_des : string
  ) {}
}

export class T_RMA_Action implements I_RMA_ACTION {

  constructor(
    public  aid : Number,
    public  action: string,
    public  text_data : string,
    public  file_gid: Number,
    public  rid: Number,
    public  ts: Date
  ) {
  }
}
