export interface I_Test {
  testid : Number,
  boarduuid: string,
  current_test : Number,
  count_pending_test : Number,
  status : string,
  ts : Date,
}

export class TestControl implements I_Test {

  constructor(
    public testid : Number,
    public boarduuid: string,
    public current_test : Number,
    public count_pending_test : Number,
    public status : string,
    public ts : Date,
  ) {
  }
}

export interface I_TestDetail{
  testid_ctl : Number,
  status: string,
  ts : Date,
  test_detail_status : string,
  result : string,
  testgroupid : string,
  testdefname : string,
  des : string,
  testid : number
}

export class TestDetail implements I_TestDetail{
  public id: Number;
  constructor(
    public testid_ctl : Number = 0,
    public status: string = '',
    public ts : Date = null,
    public test_detail_status : string = '',
    public result : string = '',
    public testgroupid : string = '',
    public testdefname : string = '',
    public des : string = '',
    public testid : number = 0
  ) {
  }

  assign(datId, dat : I_TestDetail){
    this.id = datId;
    this.testid_ctl = dat.testid_ctl;
    this.status = dat.status;
    this.ts = dat.ts;
    this.test_detail_status = dat.test_detail_status;
    this.result = dat.result;
    this.testgroupid = dat.testgroupid;
    this.testdefname = dat.testdefname;
    this.des = dat.des;
    this.testid = dat.testid;
  }
}

export interface I_TestDetail_Msg {
  status : string,
  count: number,
  test : I_TestDetail[]
}

export interface I_TestFile {
  fileid: number,
  testid_ctl : number,
  testid : number,
  foid: number,
  mime: string,
  fdes: string
}

export interface tm_TestFile {
  status : string,
  files : I_TestFile[]
}

export class TestFile_List {
  testid_ctl : number;
  testfile : I_TestFile[];
}