export interface I_TEST_DEFINITION {
    testdefid: number,
    testdefname: string,
    des: string
}

export interface tm_TEST_DEFINITION {
    status: string,
    testDefinition: I_TEST_DEFINITION[]
}

export interface I_TEST_DEF_VAR {
    testdefid: number,
    varname: string,
    varvalue: string
}

export interface tm_TEST_DEF_VAR {
    status: string,
    testDefinition: I_TEST_DEF_VAR[]
}

export interface tm_TEST_DEFINITION_DELETE_MOD {
    status: string,
    testDefinition: number
}