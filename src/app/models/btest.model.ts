export interface I_BOARD_TEST_DEF {
    testdefid : number,
    testdefname : string,
    des: string
}

export interface I_BOARD_TEST {
    testid : number,
    tesgroupid: string,
    testdefid: number,
    test_definition: I_BOARD_TEST_DEF
}

export interface tm_BOARD_TEST {
    status: string,
    boards: I_BOARD_TEST[]
}

export interface I_BOARD_TEST_VAR_DEF {
    vid: number,
    testid: number,
    varname: string,
    varvalue: string
}

export interface I_BOARD_VAR_LIST {
    testid : number,
    testgroupid: string,
    testdefid: number,
    test_var : I_BOARD_TEST_VAR_DEF
}

export interface tm_BOARD_VAR {
    status : string,
    data: I_BOARD_VAR_LIST[]
}

export interface I_BOARD_TEST {
    testid : number,
    testgroupid: string,
    testdefid: number
}

export interface tm_I_BOARD_TEST {
    status : string,
    data: I_BOARD_TEST
}

export interface tm_I_BOARD_OP {
    status : string,
    data: boolean
}