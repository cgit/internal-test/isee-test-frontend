export interface I_Upload_File_Info {
  f_id : number,
  filename: string,
  description: string,
  mime_t: string,
  data_oid: number
  f_groupid: number,
  cdate: Date
}

export interface tm_Upload_list{
  status : string,
  data : I_Upload_File_Info[]
}

export interface tm_FileDelete {
  status : string,
  result : Number
}

export class RMA_Image {

  constructor(
    public filename : string,
    public filepath : string,
    public f_id : Number
  ){}
}
