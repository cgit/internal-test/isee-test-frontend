
export interface I_Board {
  board_uuid : string,
  board_processor_id : string,
  board_creation : Date,
  bmac0 : string,
  mid: Number,
  station: string,
  factorycode: string,
  bmac1 : string,
  ofid : Number
}

export class Board implements I_Board {
  constructor(
    public board_uuid : string,
    public board_processor_id : string,
    public board_creation : Date,
    public bmac0 : string,
    public mid: Number,
    public station: string,
    public factorycode: string,
    public bmac1 : string,
    public ofid : Number
  ){
  }
}

export interface I_Board_Msg {
  status : string,
  boards : I_Board[]
}

export interface I_BoardModel {
    mid : Number,
    modelid: string,
    variant: string,
    des: string,
    testgroupid: string
}

export interface I_BoardModel_info {
  board_uuid : string,
  board_processor_id : string,
  board_creation : Date,
  bmac0 : string,
  mid: Number,
  station: string,
  factorycode: string,
  bmac1 : string,
  ofid : Number,
  board_model : I_BoardModel
}

export interface tm_BoardModel_Info {
  status : string,
  board: I_BoardModel_info
}

export interface I_BoardTestResults {
  testid_ctl : number,
  status : string,
  ts: Date
}

export interface tm_BoardTestResults {
  status: string,
  results: I_BoardTestResults[]
}

export interface tm_BoardDelete_Msg {
  status : string,
  data : number
}