export interface I_Users {
  id: Number,
  nombre: string,
  apellidos: string,
  username: string,
  email: string,
  estado: boolean,
  rol: string,
  cdate: Date
}

export interface I_User_Login {
  username : string,
  email: string,
  password: string
}

export const UserRoles = ['ADMIN', 'USER', 'DEVEL', 'OPER', 'CUSTOMER'];
export const AdminRoles = ['ADMIN', 'DEVEL'];

export class User implements I_Users {

  constructor(
    public id : Number,
    public nombre: string,
    public apellidos: string,
    public username: string,
    public email: string,
    public estado: boolean,
    public rol: string,
    public cdate: Date)
  {
    // Put constructor code here
  }
}

export class UserLogin implements I_User_Login {

  constructor(
    public username : string,
    public email: string,
    public password : string
  ){}
}

export interface I_Login_Resp {
  status : string,
  usuario: I_Users,
  token : string
}
