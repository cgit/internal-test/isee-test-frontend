
export interface I_BOARD_MODEL {
    mid : number,
    modelid: string,
    variant : string,
    des : string,
    testgroupid : string
}

export interface tm_BOARD_MODEL {
    status : string,
    boards: I_BOARD_MODEL[]
}

export interface tm_BOARD_MODEL_MOD_DEL {
    status : string;
    bModel : string;
}