import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { AppComponent } from './app.component';
import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from './layouts';
import { FooterModule, LoginModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService, StationService, 
          IoeventsService, StatsService, ModelService, TestdefinitionService, BtestService } from './shared/services';
import { AppRoutingModule } from './app-routing.module';
import { environment } from 'src/environments/environment';


const config : SocketIoConfig = { url: environment.URL_apiServer, options: { origin: '*', transport : ['websocket'] } };

@NgModule({
  declarations: [
    AppComponent,    
  ],
  imports: [
    BrowserModule,
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,    
    LoginModule,
    AppRoutingModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [AuthService, ScreenService, AppInfoService, StationService, IoeventsService, StatsService, ModelService, TestdefinitionService, BtestService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
