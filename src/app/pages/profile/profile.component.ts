import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { AuthService } from 'src/app/shared/services';
import { I_Users } from '../../models/user.model'
import { FormGroup, FormBuilder, FormControl, AbstractControl, Validators, NgForm } from '@angular/forms';
import * as _ from "underscore";
import Swal from 'sweetalert2';
import { stringify } from 'querystring';

@Component({
  templateUrl: 'profile.component.html',
  styleUrls: [ './profile.component.scss' ]
})
export class ProfileComponent implements OnInit, OnDestroy {
  user: I_Users;
  colCountByScreen: object;
  loadingVisible :boolean;
  myForm: FormGroup;  
  message: String;

  constructor(private authService: AuthService, private fb: FormBuilder) {
    this.user = authService.user;
    this.colCountByScreen = {
      xs: 1,
      sm: 2,
      md: 3,
      lg: 4
    };
    this.CreateForm();
    this.loadingVisible = false;
  }

  CreateForm(){
    this.myForm = this.fb.group({
      nombre: [this.user.nombre],
      apellidos: [this.user.apellidos],
      email: [this.user.email]
    });
  }

  ngOnInit(): void {
    console.log("Profile ngOnInit");
  }

  @HostListener('unloaded')
  ngOnDestroy(){
    console.log('Items destroyed');

  }

  onFormSubmit (){
    let data = {};
    if (this.myForm.value.nombre != this.user.nombre)
      data['nombre'] = this.myForm.value.nombre;
    if(this.myForm.value.apellidos != this.user.apellidos)
      data['apellidos'] = this.myForm.value.apellidos;
    if(this.myForm.value.email != this.user.email)
      data['email'] = this.myForm.value.email;
    if(Object.keys(data).length > 0){
      this.loadingVisible = true;
      this.message = 'Updating User Information ...';
      this.authService.modifyMyOwnUser(data).subscribe( resp => {
        this.loadingVisible = false;
        console.log(resp);
        if(resp.status == "ok"){
          if(resp.users[0] == 1){
            this.authService.updateMyOwnUser(resp.users[1][0].nombre, resp.users[1][0].apellidos, resp.users[1][0].email)
            this.user = this.authService.user;
            Swal.fire('ok', "Modification Done", 'success');
          }
        }
        else{
          Swal.fire('Error', "Modification Error", 'error');
        }
      }, errorMessage => {
        this.loadingVisible = false;
        Swal.fire({
          icon: 'error',
          title: 'Ops...',
          text: errorMessage
        });
        this.myForm.reset();
        this.myForm.patchValue(this.user);
      });
    }
    else{
      Swal.fire('Warning', "Nothing to change", 'warning');
    }
  }

  onChangePassword(event){
    Swal.fire({
      title: 'Enter your password',
      input: 'password',
      //text: 'You will not be able to recover this imaginary file!',
      inputPlaceholder: 'Enter your password',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, change my password!',
      cancelButtonText: 'No, keep it',
      showLoaderOnConfirm: true,
    }).then((result) => {
      if (result.value) {        
        this.loadingVisible = true;
        this.message = 'Updating Password ...';
        this.authService.modifyMyOwnPassword(result.value as String).subscribe( resp => {
          this.loadingVisible = false;
          Swal.fire(
            'Password Changed!',            
            'success'
          )  
        }, errorMessage => {
          this.loadingVisible = false;
          Swal.fire({
            icon: 'error',
            title: 'Ops...',
            text: errorMessage
          });
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {}
    })
  }
}
