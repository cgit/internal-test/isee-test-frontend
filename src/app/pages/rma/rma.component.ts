import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, AbstractControl, Validators, NgForm } from '@angular/forms';
import * as _ from "underscore";
import Swal from 'sweetalert2';

import { T_RMA, tm_RMA_List, T_CREATE_RMA, T_RMA_Action, tm_RMA_DeleteAction } from 'src/app/models/rma.model';
import { tm_Upload_list, RMA_Image, tm_FileDelete } from 'src/app/models/upload.model';
import { RmaService, UploadService } from '../../shared/services/index';
import { DxCircularGaugeComponent } from 'devextreme-angular';
import { faCheckCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

import notify from 'devextreme/ui/notify';

function getWindow(){
  return window;
}

@Component({
  selector: 'app-rma',
  templateUrl: './rma.component.html',
  styleUrls: ['./rma.component.scss']
})
export class RmaComponent implements OnInit {

  rmalist : T_RMA[];
  newRma : T_CREATE_RMA;
  rmaSelected : T_RMA;
  rmaAction : T_RMA_Action[];
  rmaActions: string[];
  rmaActionsFiltered: string[];
  rmaActionSelected : T_RMA_Action;
  RmaActionFile : RMA_Image[] = [];

  applyFilterTypes: any;
  showFilterRow: boolean;
  showHeaderFilter: boolean;
  resizingModes: string[] = ["nextColumn", "widget"];
  columnResizingMode: string;

  loadingVisible : boolean = false;
  createRmaFormVisible : boolean = false;
  rmaDeleteVisible : boolean = false;
  RmaImagesVisible : boolean = false;
  createRmaActionFormVisible : boolean = false;
  rmaDeleteActionVisible : boolean = false;

  myCreateRmaForm: FormGroup;
  myCreateRmaActionForm : FormGroup;

  ImageContextMenuItems : any;

  slideshowDelay : Number = 1000;
  filelist : File[];

  faCheckCircle = faCheckCircle;
  faExclamationCircle = faExclamationCircle;

  _window : any;

  @ViewChild("galleryImage", {read: ElementRef}) galleryImage: ElementRef;

  constructor(private rmaService : RmaService, private fb: FormBuilder, private uploadService : UploadService, private router: Router) {
    this.showFilterRow = true;
    this.showHeaderFilter = true;
    this.applyFilterTypes = [{
                  key: "auto",
                  name: "Immediately"
                },
                {
                  key: "onClick",
                  name: "On Button Click"
                }];
    this.columnResizingMode = this.resizingModes[1];
    this._window = getWindow()
    this.newRma = new T_CREATE_RMA('', '', '', '', '');
    this.myCreateRmaForm = this.fb.group({
      rma_id: [this.newRma.rma_id],
      factory_code: [this.newRma.factory_code],
      customer: [this.newRma.customer],
      assigned_to: [this.newRma.assigned_to],
      rma_des: [this.newRma.rma_des],
    });

    this.myCreateRmaActionForm = this.fb.group({
      action: [],
      text_action: [],
    });

    this.ImageContextMenuItems = [
      {
        text: 'Delete'
      },
    ]
  }

  ngOnInit() {
    this.updateRMATable();
    this.rmaService.getActionList().subscribe( resp => {
      this.rmaActions = [];
      this.rmaActionsFiltered = [];
      let data = resp.action_list;
      for(let key in data){
        this.rmaActions.push(data[key]['action']);
        if(data[key]['action'] != 'CREATE_RMA' && data[key]['action'] != 'OPEN_RMA' && data[key]['action'] != 'RMA_RESOLUTION')
          this.rmaActionsFiltered.push(data[key]['action']);
      }
      console.log(resp);
    }, errorMessage => {
      console.log(errorMessage);
    });
  }

  currentFilter(){
    return this.applyFilterTypes[0];
  }

  OnImageClick(event){
    if(event.itemData['text'] == 'Delete'){
      this.uploadService.deleteFile(this.galleryImage.nativeElement.alt).subscribe( delFileresp => {
        if(delFileresp.status == 'ok' && delFileresp.result == 1){
          this.RmaActionFile = []
          this.uploadService.getFilelist_by_groupid( this.rmaActionSelected.file_gid ).subscribe( (res  : tm_Upload_list) => {
            console.log(res);
            res.data.forEach( v => {
              let Image = new RMA_Image(v['filename'], this.uploadService.getfileurl(v), v['f_id']);
              this.RmaActionFile.push(Image);
              console.log(v);
            })
          }, errorMessage => {
          console.log(errorMessage);
          });
        }
      });
    }
  }

  updateRMATable(){
    this.rmaService.getRmalist().subscribe((data: tm_RMA_List) => {
      if(data.status == "ok"){
        this.rmalist = data.rma;
      }
      console.log(data);
    });
  }

  ResfeshTablesWithSelected(){
    this.rmaService.getRmalist().subscribe((data: tm_RMA_List) => {
      if(data.status == "ok"){
        this.rmalist = data.rma;
        this.rmalist.forEach( data => {
          if(data.rid == this.rmaSelected.rid){
            this.rmaSelected = data;
          }
          // this.rmaActionSelected =
          console.log(data);
        })
        this.RefreshActionTable();
      }
      console.log(data);
    });
  }

  RefreshActionTable(){
    this.rmaService.getRMAActionlist(this.rmaSelected.rid).subscribe (resp => {
      this.rmaAction = resp.rma;
      console.log(resp);
    } , errorMessage => {
      console.log(errorMessage);
    })
  }

  onCellClick(e) {
    if(e.row == undefined)
      return;
    if (e.row.rowType == "data") {
      this.rmaSelected = e.row.data;
      console.log(this.rmaSelected);
      this.rmaService.getRMAActionlist(this.rmaSelected.rid).subscribe (resp => {
        this.rmaAction = resp.rma;
        console.log(resp);
      } , errorMessage => {
        console.log(errorMessage);
      })
    }
  }

  onDeleteRmaClick(event){
    if(this.rmaSelected == undefined)
      Swal.fire('Error', "Please, select RMA First", 'error');
    else{
      this.rmaDeleteVisible = true;
    }
  }

  deleteRMA(){
    this.rmaDeleteVisible = false;
    this.rmaService.deleteRMA(this.rmaSelected.rid).subscribe( resp => {
      console.log(resp);
      this.updateRMATable();
      this.rmaAction = [];
    }, errorMessage => {
      console.log(errorMessage);
    })
  }

  deleteRMAAction(){
    // this.rmaService.deleteRMA
    this.rmaService.DeleteRMAAction(this.rmaSelected.rid, this.rmaActionSelected.aid).subscribe( (delActionResp : tm_RMA_DeleteAction) => {
      if(delActionResp.status == 'ok'){
        if(delActionResp.result == 1){
          this.updateRMATable();
          this.RefreshActionTable();          
        }
      }
      this.rmaDeleteActionVisible = false;
    }, delActionRespError => {
      notify({ message: delActionRespError, shading: true }, "error", 3000);
      this.rmaDeleteActionVisible = false;
    })
  }

  onCreateRmaClick(event){
    this.createRmaFormVisible = true;
  }

  onCreateRmaFormSubmit(){
    this.rmaService.createRMA(this.myCreateRmaForm.value).subscribe(resp => {
      console.log(resp);
      this.updateRMATable();
    } , errorMessage => {
      console.log(errorMessage)
    });
    this.myCreateRmaForm.reset();
    this.createRmaFormVisible = false;
  }

  onCreateRmaActionFormSubmit(){
    this.rmaService.setRMAAction(
      this.rmaSelected.rid, this.myCreateRmaActionForm.value.action, this.myCreateRmaActionForm.value.action_text).subscribe( newActionResp => {
        console.log(newActionResp);
        this.updateRMATable();
        this.RefreshActionTable();
      }, RmaActionError => {
        console.log(RmaActionError);
      });
    this.myCreateRmaActionForm.reset();
    this.createRmaActionFormVisible = false;
  }

  onRowUpdating(event){
    this.rmaService.modifyRMA(event.key.rid, event.newData).subscribe( resp => {
      console.log(resp);
    } , errorMessage => {
      Swal.fire('Error', errorMessage, 'error');
      event.cancel = true;
    });
  }

  onActionRowUpdating(event){
    this.rmaService.modifyRMA_Action(event.key.rid, event.key.aid ,event.newData).subscribe( resp => {
      console.log(resp);
    } , errorMessage => {
      Swal.fire('Error', errorMessage, 'error');
      event.cancel = true;
    });
  }

  refreshFiles(withNotify : boolean)
  {
    this.RmaActionFile = [];    
    this.uploadService.getFilelist_by_groupid( this.rmaActionSelected.file_gid ).subscribe( (res  : tm_Upload_list) => {
      console.log(res);
      res.data.forEach( v => {
        let Image = new RMA_Image(v['filename'], this.uploadService.getfileurl(v), v['f_id']);
        this.RmaActionFile.push(Image);
        console.log(v);
      })
      if(withNotify)
        notify({ message: 'Upload Complete', shading: true }, "success", 1000);
    });
  }


  onImageFormSubmit(event){
    this.rmaService.getRMAFileGroup(this.rmaActionSelected.rid, this.rmaActionSelected.aid).subscribe( resp => {
      this.uploadService.upload_file(this.filelist[0], resp.file_gid).subscribe( uploadResp =>{
        this.refreshFiles(true);
      } , uploadErrorMsg => {
        console.log(uploadErrorMsg.message);
        // notify(uploadErrorMsg.message, "error", 3000);
        notify({ message: uploadErrorMsg.message, shading: true }, "error", 1000);
      })
    } , errorMessage => {
      console.log(errorMessage);
      notify({ message: errorMessage, shading: true }, "error", 1000);
    });
    // console.log(event.target[0].files);
    // console.log(event);
  }

  canSubmit(){
    if(this.filelist == undefined || this.filelist.length == 0)
      return true;
    return false;
  }

  onfileuploader(event){
    console.log(event);
    this.filelist = event.value;
  }

  onContextMenuPreparing(e) {
    if (e.row.rowType == "data") {
      this.rmaSelected = e.row.data;
      if(this.rmaSelected.rma_resolution.rma_status == "OPEN"){
        e.items = [
          {
            text: "Add RMA", onItemClick: () => {
              this.onCreateRmaClick(null);
            }
          },
          {
            text: "Delete RMA", onItemClick: () => {
              this.onDeleteRmaClick(null);
            }
          },
          {
            text: "Close RMA", onItemClick: () => {
              Swal.fire({
                title: 'Close RMA',
                input: 'textarea',
                inputPlaceholder: 'Close RMA Message...',
                showCancelButton: true
              }).then( res => {
                if (res.isConfirmed)
                  this.rmaService.CloseRMA(this.rmaSelected.rid, res.value).subscribe( closeRMAresp => {
                    if(closeRMAresp.status == 'ok'){
                      this.ResfeshTablesWithSelected();
                    }
                  });
              })
            }
          },
          {            
            text: "Show Board Details", onItemClick: () => {
              if(e.row.data.board_uuid != null)
                this.router.navigate(['/board-detail/' + e.row.data.board_uuid]);                
            }            
          }
        ]
      }
      else{
        e.items = [
          {
            text: "Add RMA", onItemClick: () => {
              this.onCreateRmaClick(null);
            }
          },{
            text: "Delete RMA", onItemClick: () => {
              this.onDeleteRmaClick(null);
            }
          },
          {
            text: "Open RMA", onItemClick: () => {
              Swal.fire({
                title: 'Reopen RMA',
                input: 'textarea',
                inputPlaceholder: 'Reopen RMA Message...',
                showCancelButton: true
              }).then( res => {
                if (res.isConfirmed)
                  this.rmaService.OpenRMA(this.rmaSelected.rid, res.value).subscribe( closeRMAresp => {
                    if(closeRMAresp.status == 'ok'){
                      this.ResfeshTablesWithSelected();
                    }
                  });
              })
            }
          },
          {            
            text: "Show Board Details", onItemClick: () => {
              if(e.row.data.board_uuid != null)
                this.router.navigate(['/board-detail/' + e.row.data.board_uuid]);
            }            
          }
        ]
      }
      console.log(e.row.data);
    }
  }

  onActionContextMenuPreparing(e){
    if (e.row != undefined && e.row.rowType == "data") {
      this.rmaActionSelected = e.row.data;
      if(this.rmaSelected.rma_resolution.rma_status == "OPEN"){
        console.log(e.row.data);
        e.items = [
          {
            text: "Create Action", onItemClick: () => {
              this.createRmaActionFormVisible = true;
            }
          },
          {
            text: "Delete Action", onItemClick: () => {
              if(this.rmaActionSelected.action == 'RMA_DESCRIPTION' ||
                  this.rmaActionSelected.action == 'TEST_CASE' ||
                  this.rmaActionSelected.action == 'INSPECT_BOARD'|| 
                  this.rmaActionSelected.action == 'NOT_ISSUE_FOUND'|| 
                  this.rmaActionSelected.action == 'BOARD_REPLACEMENT')
                this.rmaDeleteActionVisible = true;
            }
          },
          {
            text: "Add Images", onItemClick: () => {
              this.RmaActionFile = [];
              if(this.rmaActionSelected.file_gid != -1){
                this.uploadService.getFilelist_by_groupid( this.rmaActionSelected.file_gid ).subscribe( (res  : tm_Upload_list) => {
                  console.log(res);
                  res.data.forEach( v => {
                    let Image = new RMA_Image(v['filename'], this.uploadService.getfileurl(v), v['f_id']);
                    this.RmaActionFile.push(Image);
                    console.log(v);
                  })
                }, errorMessage => {
                  console.log(errorMessage);
                })
              }
              this.RmaImagesVisible = true;
            }
          },
        ]
      }
      else{
        if(this.rmaActionSelected.file_gid != -1){
          e.items = [
           {
              text: "View Images", onItemClick: () => {
                this.refreshFiles(false);
                this.RmaImagesVisible = true;
              }
           }
          ]
        }
      }
    }
  }

  checkIcon(data, IconTypeCheck){
    if(data.data.rma_resolution.rma_status == 'CLOSE' && IconTypeCheck == 'faCheckCircle')
      return true;
    else if(data.data.rma_resolution.rma_status == 'OPEN' && IconTypeCheck == 'faExclamationCircle')
      return true;
    else
      return false;
  }

  checkReplaceIcon(data, IconTypeCheck){
    if(data.data.replaceboard == true && IconTypeCheck == 'faCheckCircle')
      return true;
    else
      return false;
  }

}
