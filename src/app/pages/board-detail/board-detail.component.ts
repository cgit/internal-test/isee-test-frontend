import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DxTabsModule, DxSelectBoxModule,DxButtonModule } from 'devextreme-angular';
import { BoardsService } from 'src/app/shared/services';
import { AuthService } from 'src/app/shared/services';
import { Board, tm_BoardModel_Info, I_BoardModel_info, I_BoardTestResults, tm_BoardTestResults, tm_BoardDelete_Msg } from 'src/app/models/board.model';
import { I_TestDetail_Msg, I_TestDetail, TestDetail, tm_TestFile, TestFile_List, I_TestFile } from 'src/app/models/test.model';
import { I_Users } from '../../models/user.model'
import { faObjectGroup } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.scss']
})
export class BoardDetailComponent implements OnInit {

  board_id : string;
  myBoard: I_BoardModel_info;
  myTestDetail : TestDetail[] = [];
  myTestDetFiles: TestFile_List[] = [];
  myTestResults : I_BoardTestResults[] = [];

  myTestDetailSelected : TestDetail;
  myTestDetFilesSelected: I_TestFile[] = [];

  TotaltestCount : number = 0;
  TotalTestFailed: number = 0;
  TotalTestOk: number = 0;
  LastTestID : number = 0;
  LastTestResult: boolean = false;

  faCheckCircle = faCheckCircle;
  faExclamationCircle = faExclamationCircle;

  message : string;
  loadingVisible : boolean = false;

  buttonOptions: any = {
    text: 'Delete Board',
    type: 'danger',
    onClick: data => {          
      this.OnClickDeleteBoard();
    }
    // useSubmitBehavior: true,
  };  

  bVisibleDel : Boolean = false;

  constructor(private actRoute: ActivatedRoute, private router: Router, private boardService : BoardsService, private authService: AuthService) {
    this.board_id = this.actRoute.snapshot.params.b_id;
    if(authService.IsAdmin())
      this.bVisibleDel = true;
  }

  ngOnInit() {
    this.boardService.getBoardDetail(this.board_id).subscribe( (boardResp: tm_BoardModel_Info) => {
      this.myBoard = boardResp.board[0];
      this.boardService.getBoardAllTestDetail(this.board_id, -1, 0, 'ts', 'DESC').subscribe( (resp : I_TestDetail_Msg) => {
        let countid = 1;
        resp.test.forEach( dat => {
          let testDat = new TestDetail();
          testDat.assign(countid, dat);
          countid++;
          this.myTestDetail.push( testDat );
          if(testDat.test_detail_status == 'TEST_FAILED')
            this.TotalTestFailed++;
          else
            this.TotalTestOk++;
        });
        this.boardService.getBoardTestResults(this.board_id).subscribe( (tResults : tm_BoardTestResults) => {
          this.myTestResults = tResults.results;
          this.getlistTestid_ctl().forEach( (testid_ctl : number) => {
            this.boardService.getBoardTestFile(this.myBoard.board_uuid, testid_ctl).subscribe( (flistresponse : tm_TestFile) => {
              let mytlist = new TestFile_List();
              mytlist.testid_ctl = testid_ctl;
              mytlist.testfile = flistresponse.files;
              this.myTestDetFiles.push(mytlist);
              this.calculate_statistics();  
            } , flistError => {
              console.log(flistError);
            });          
          })          
        }, tResultsError => {
          console.log(tResultsError);
        })
      });
    }, boardRespError => {
      console.log(boardRespError);
      this.router.navigate(['/login']);
    });
  }

  getlistTestid_ctl () : Set<Number> {    
    let b : number[] = [];    
    this.myTestDetail.forEach( (data : TestDetail) => {      
      b.push( Number(data.testid_ctl) );
    });
    let ulist = new Set(b);    
    return ulist;
  }

  calculate_statistics(){
    this.TotaltestCount = this.getlistTestid_ctl().size;
    if(this.myTestResults[0] != undefined){
      this.LastTestID = this.myTestResults[0].testid_ctl;
      this.LastTestResult = (this.myTestResults[0].status == "TEST_BOARD_OK");      
    }
  }

  onContextMenuPreparing(e){
    if (e.row != undefined && e.row.rowType == "data") {
      this.myTestDetailSelected = e.row.data;
      this.myTestDetFilesSelected = [];
      this.myTestDetFiles.forEach( (testfiles : TestFile_List) => {
        if( testfiles.testid_ctl == this.myTestDetailSelected.testid_ctl ){
          testfiles.testfile.forEach( (f : I_TestFile) => {
            if(f.testid == this.myTestDetailSelected.testid){
              this.myTestDetFilesSelected.push(f);              
            }
          })          
        }
      });            
      e.items = [];
      this.myTestDetFilesSelected.forEach( (f : I_TestFile) => {
        e.items.push( this.createItemDownloadMenu('Download: ' + f.fdes, f.mime, f.fileid ,f.foid, f.testid_ctl, f.testid) );
      });      
    }
  }

  OnDownloadClick(itemName, mime, fileid, foid, testid_ctl, testid){    
    this.boardService.getBoardTestResultFile(fileid).subscribe( respFile => {
      console.log(respFile);
      let dataType = respFile.type;
      let binaryData = [];
      binaryData.push(respFile);
      let downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
      let filename = `${this.myTestDetailSelected.testdefname}-${testid_ctl}-${testid}-${fileid}`
        downloadLink.setAttribute('download', filename);
      document.body.appendChild(downloadLink);
      downloadLink.click();      
    }, respError => {
      console.log(respError);
    });
  }

  createItemDownloadMenu(itemName, mime, fileid, foid, testid_ctl, testid){
    let ItemMenu = {};
    ItemMenu['text'] = itemName;
    ItemMenu['onItemClick'] = () => {this.OnDownloadClick(itemName, mime, fileid, foid, testid_ctl, testid)}
    return ItemMenu;
  }

  onSelectionChanged(e){
    console.log(e);
  }  

  onShowBoardsClick($e){
    this.router.navigate(['/board']);
  }

  OnClickDeleteBoard(){
    console.log("OnClick - Delete: " + this.board_id);

    Swal.fire(
      {
        title: 'Delete Board',
        text: 'Do you want to Delete this Board?',      
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Remove it!',
        cancelButtonText: 'No, keep it',
        showLoaderOnConfirm: true,
      }
    ).then( result => {
          if (result.value) {
            this.message = "delete board ... ";
            this.loadingVisible = true;
            this.boardService.deleteBoard(this.board_id).subscribe( (response : tm_BoardDelete_Msg) => {
              this.loadingVisible = false;
              if(response.status == "ok"){
                if(response.data == 1){
                  Swal.fire(
                    'Board Removed!',            
                    'success'
                  );      
                }
                else{
                  Swal.fire(
                    'Board Not found!'
                  );      
                }
              }
              else{
                Swal.fire(
                  'Board delete Error!',            
                  'error'
                );      
              }
              this.router.navigate(['/board']);
            }, respError => {
              console.log(respError);
            });            
          }
      }
    );
  }

}
