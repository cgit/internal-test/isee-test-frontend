import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { I_BOARD_MODEL } from 'src/app/models/bmodel.model';
import { I_BOARD_TEST, I_BOARD_VAR_LIST } from 'src/app/models/btest.model';
import { I_TEST_DEFINITION } from 'src/app/models/tdef.model';
import { BtestService, ModelService, TestdefinitionService } from 'src/app/shared/services';

@Component({
  selector: 'app-boardtest',
  templateUrl: './boardtest.component.html',
  styleUrls: ['./boardtest.component.scss']
})
export class BoardtestComponent implements OnInit {

  btest_ds : I_BOARD_TEST[] = [];
  bModels_ds : I_BOARD_MODEL[] = [];
  bTestdef_ds : I_TEST_DEFINITION[] = [];
  bTestVars_ds : I_BOARD_VAR_LIST[] = [];
  showHeaderFilter : boolean = true;
  testidSelected : number = -1;
  VarAllowAdding : boolean = false;

  @ViewChild('btest_grid', { static: false }) dataGrid: DxDataGridComponent;

  constructor(private bTestService : BtestService, private modService : ModelService, private testdef: TestdefinitionService) { }

  ngOnInit() {
    // Get Board test List Variables
    this.bTestService.getBoardTestList().subscribe( resp => {      
      this.btest_ds = resp.boards;
      console.log(resp);
    }, error => {
      console.log(error);
    })

    // Get Board List MODELS
    this.modService.getModelTable().subscribe( resp => {
      this.bModels_ds = resp.boards;
    }, error => {
      console.log(error);
    });

    // Get test List Test
    this.testdef.getTestDefinitions().subscribe( resp => {
      this.bTestdef_ds = resp.testDefinition;
    }, error => {
      console.log(error);
    });

    // In some cases grid is not refres with all data
    this.dataGrid.instance.refresh().then(e => {}).catch(e => {})

  }

  locate_testid( name : string){
    for(let i = 0; i < this.bTestdef_ds.length; i++){
      let j : I_TEST_DEFINITION;
      j = this.bTestdef_ds[i];
      if(j.testdefname == name)
        return j.testdefid.toString();
    }
    return "-1";
  }

  locate_testdes(name : string){
    for(let i = 0; i < this.bTestdef_ds.length; i++){
      let j : I_TEST_DEFINITION;
      j = this.bTestdef_ds[i];
      if(j.testdefname == name)
        return j.des;
    }
    return "";
  }

  onRowInserting(e){    
    e.cancel = new Promise( (resolve, reject) => {
      let params = e.data;
      let testdefid = this.locate_testid( params.test_definition.testdefname );
      this.bTestService.CreateBoardTest( params.testgroupid, testdefid).subscribe( resp => {
        e.data.testid = resp.data.testid;
        e.data.test_definition.des = this.locate_testdes(params.test_definition.testdefname);        
        resolve();
      }, error => {
        reject(error);
      });      
    });
  }

  onRowUpdating(e){
    // Nothing to do
  }

  onRowRemoving(e){
    e.cancel = new Promise( (resolve, reject) => {
      this.bTestService.DeleteBoardTest(e.key).subscribe( resp => {
        resolve();
      }, error => {
        reject(error);
      });
    });
  }

  onVarRowInserting(e){
    e.cancel = new Promise( (resolve, reject) => {
      let varname = e.data.test_var.varname;
      let varvalue = e.data.test_var.varvalue;
      this.bTestService.createNewVar(this.testidSelected, varname, varvalue).subscribe( resp => {        
        e.data.test_var.vid = resp.data.vid;
        e.data.test_var.testid = resp.data.testid;
        resolve();
      }, error => {
        reject(error);
      });
    });    
  }

  onVarRowUpdating(e){
    e.cancel = new Promise( (resolve, reject) => {      
      this.bTestService.updateVar(this.testidSelected, e.key.test_var.vid, e.newData.test_var).subscribe( resp => {        
        resolve();
      }, error => {
        reject(error);
      });
    });        
  }

  onVarRowRemoving(e){
    e.cancel = new Promise( (resolve, reject) => {      
      this.bTestService.deleteVar(this.testidSelected, e.key.test_var.vid).subscribe( resp => {        
        resolve();
      }, error => {
        reject(error);
      });
    });        
  }

  onCellClick(e){
    this.bTestService.getBoardTestVars(e.key).subscribe( resp => {      
      this.bTestVars_ds = resp.data;
      this.testidSelected = e.key;
      this.VarAllowAdding = true;
    }, error => {
      this.bTestVars_ds = [];
      this.VarAllowAdding = false;
      console.log(error);
    });
  }

  onEditorPreparing(e) {      
    if (e.parentType !== "dataRow" || e.dataField !== "test_definition.des") {  
        return;  
    }      
    if (e.dataField === "test_definition.des") {  
        e.editorOptions.disabled = true;  
    } 
  }
  
  onSelectionChanged(e){
    console.log(e);
    /* this.testidSelected = e.currentSelectedRowKeys[0] */
  }

  onFocusedRowChanging(e){
    console.log(e);
  }

  onContentReady(e){
    console.log(e);
    this.bTestVars_ds = [];
    this.VarAllowAdding = false;
  }




}
