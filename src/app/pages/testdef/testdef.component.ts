import { Component, OnInit } from '@angular/core';
import { I_TEST_DEFINITION, I_TEST_DEF_VAR } from 'src/app/models/tdef.model';
import { TestdefinitionService } from 'src/app/shared/services';

@Component({
  selector: 'app-testdef',
  templateUrl: './testdef.component.html',
  styleUrls: ['./testdef.component.scss']
})
export class TestdefComponent implements OnInit {

  testdef_ds : I_TEST_DEFINITION[] = [];
  testdef_var_ds : I_TEST_DEF_VAR[] = [];

  tdefSelected : number = -1;

  constructor(private testdef: TestdefinitionService) { }

  ngOnInit() {

    this.testdef.getTestDefinitions().subscribe( resp => {
      this.testdef_ds = resp.testDefinition;      
    });

  }

  onRowInserting(e){
    e.cancel = new Promise( (resolve, reject) => {
      this.testdef.CreateTestDefinition(e.data).subscribe (resp => {
        e.data = resp.testDefinition;
        resolve();
      }, error => {
        reject();
      });  
    });

  }

  onRowUpdating(e){
    e.cancel = new Promise( (resolve, reject) => {
      this.testdef.ModifyTestDefinition(e.key, e.newData).subscribe (resp => {
        if(resp.testDefinition == 1)
          resolve();
        else 
          reject();
      }, error => {
        reject();
      });  
    });    
  } 

  onRowRemoving(e){
    e.cancel = new Promise( (resolve, reject) => {
      this.testdef.DeleteTestDefinition(e.key).subscribe (resp => {
        if(resp.testDefinition == 1){
          resolve();
          this.testdef_var_ds = [];
          this.tdefSelected = -1;
        }
        else 
          reject();
      }, error => {
        reject();
      });  
    });    
  }

  onCellClick(e){
    console.log(e.key);
    this.testdef.getTestDefVars(e.key).subscribe( resp => {
      this.tdefSelected = e.key;
      this.testdef_var_ds = resp.testDefinition;      
    });
  }

  onVarRowInserting(e){    
    e.cancel = new Promise( (resolve, reject) => {
      this.testdef.CreateTestDefinitionVar(this.tdefSelected, e.data).subscribe (resp => {
        e.data = resp.testDefinition;
        resolve();
      }, error => {
        reject();
      });  
    });    
  }

  onVarRowUpdating(e){
    e.cancel = new Promise( (resolve, reject) => {
      let data = {};
      data['varname'] = e.key.varname;
      data['varvalue'] = e.newData.varvalue;
      this.testdef.ModifyTestDefinitionVar(e.key.testdefid, data).subscribe (resp => {
        if(resp.testDefinition == 1)
          resolve();
        else 
          reject();
      }, error => {
        reject();
      });  
    });        
  }

  onVarRowRemoving(e){
    e.cancel = new Promise( (resolve, reject) => {
      let data = {};
      data['varname'] = e.key.varname;      
      this.testdef.DeleteTestDefinitionVar(e.key.testdefid, data).subscribe (resp => {
        if(resp.testDefinition == 1)
          resolve();
        else 
          reject();
      }, error => {
        reject();
      });  
    });            
  }

  onEditorPreparing(e) {  
    if (e.parentType !== "dataRow" || e.dataField !== "varname") {  
        return;  
    }  
    if (e.row.data.varname) {  
        e.editorOptions.disabled = true;  
    }  
}  


}
