import { Component, OnInit } from '@angular/core';
import { I_BOARD_MODEL } from 'src/app/models/bmodel.model';
import { ModelService } from 'src/app/shared/services';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.scss']
})
export class ModelComponent implements OnInit {

  events: Array<string> = [];
  bModelsDataSource : I_BOARD_MODEL[] = [];

  constructor( private modService : ModelService) { }

  ngOnInit() {
    
    this.modService.getModelTable().subscribe( resp => {
      this.bModelsDataSource = resp.boards;
      console.log(resp);
    });

  }

  logEvent(eventName) {
    this.events.unshift(eventName);
  }

  clearEvents() {
    this.events = [];
  }

  onRowInserting( e ){
    e.cancel = new Promise( (resolve, reject) => {
      let params = e.data;
      this.modService.CreateNewModel(params).subscribe( resp => { 
        resolve();
        console.log(resp);
      }, error => { 
        reject(error);
        console.log(error);
      })  
    });
  }

  onRowUpdating ( e ){
    e.cancel = new Promise( (resolve, reject) => {
      let params = e.newData;
      this.modService.ModifyModel(e.key, params).subscribe( resp => {        
          resolve();
        console.log(resp);
      }, error => { 
        reject(error);
        console.log(error);
      })  
    });
  }

  onRowRemoving ( e ){
    e.cancel = new Promise( (resolve, reject) => {      
      this.modService.DeleteModel(e.key).subscribe( resp => {        
          resolve();
        console.log(resp);
      }, error => { 
        reject(error);
        console.log(error);
      })  
    });
  }


}
