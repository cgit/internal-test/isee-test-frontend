import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { IoeventsService, StationService, BoardsService } from 'src/app/shared/services';
import { Observable, Subscription } from 'rxjs';
import { IOMsg } from '../../models/event.model';
import { Station, StationInfo, StationStatus } from '../../models/station.model';
import { I_TestDetail_Msg, TestDetail, I_TestDetail } from '../../models/test.model';

import { DxContextMenuComponent } from 'devextreme-angular';
import { trigger } from "devextreme/events";
import { ClassField } from '@angular/compiler';

import { faCheckCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit, OnDestroy {

  // stations :  Station [];
  myStation : StationInfo [];
  myIOEvent : Subscription;
  myStations : Observable<Station[]>;
  menuItems: any[];
  stationSelected : StationInfo;
  stBoardSelected : TestDetail[];

  faCheckCircle = faCheckCircle;
  faExclamationCircle = faExclamationCircle;

  loadingVisible : boolean = false;
  popupErrorVisible : boolean = false;

  showHeaderFilter: boolean;

  constructor( private ioEvents: IoeventsService, private stService : StationService, private stBoard: BoardsService, private router: Router) {
    this.stationSelected = null;
    this.myStation = [];
    this.stBoardSelected = [];
    this.showHeaderFilter = true;
  }

  ngOnInit() {
    // Subscribe to 30 seconds update
    this.myStations = this.stService.getMyStations();
    this.myStations.subscribe( (data: Station[]) => {
      // this.stations = data;
      this.myStation = [];
      data.forEach( element => {
        let stInfo : StationInfo;
        if (element.heartbeat.f_timeout > 0){
          stInfo = new StationInfo( "faCheckCircle", element );
        }
        else{
          stInfo = new StationInfo( "faExclamationCircle", element );
        }
        this.myStation.push(stInfo);
      })
    });

    this.myIOEvent = this.ioEvents.getMessage().subscribe( resp => {
      if(resp.id == 'N_STATION_STATE'){
        this.stService.forceUpdate();
      }
    });
    
    this.stService.forceUpdate();
  }

  ngOnDestroy(){
    this.stService.freeMyStations(this.myStations);
    this.myIOEvent.unsubscribe();
  }

  contextMenuItemClick(e){
    console.log("contextMenuItemClick");
    console.log("e");
  }

  onContextMenuPreparing(e) {
    if (e.row.rowType == "data") {
      this.stationSelected = e.row.data;
        if(this.stationSelected.station.state == "STATION_ERROR"){
          e.items = [
            {
              text: "error", onItemClick: () => {
                this.loadingVisible = true;
                this.stService.getStationHostVar(this.stationSelected.station.hostname, 'STATION_ERROR').subscribe( resp => {
                    this.stationSelected.StationError = resp.vars[0].value;
                    this.loadingVisible = false;
                    this.popupErrorVisible = true;
                    console.log(resp);
                });
              }
            },
        ];
      }
      else if (this.stationSelected.station.state == "TESTS_RUNNING"){
        e.items = [
          {
            text: "Refresh", onItemClick: () => {
              this.loadingVisible = true;
              this.stBoard.getBoardtestDetail(this.stationSelected.station.board_uuid, this.stationSelected.station.testid_ctl , -1, 0, true).subscribe( resp => {
                  this.stBoardSelected = [];
                  resp.test.forEach( element => {
                    let t = new TestDetail(
                      element.testid_ctl,
                      element.status,
                      element.ts,
                      element.test_detail_status,
                      element.result,
                      element.testgroupid,
                      element.testdefname,
                      element.des
                    )
                    this.stBoardSelected.push(t);
                  })
                  this.loadingVisible = false;
                  console.log(resp);
              });
            }
          },
          {
            text: "Show Board Details", onItemClick: () => {
              this.router.navigate(['/board-detail/' + e.row.data.station.board_uuid]);
            }
          },
        ];
      }
      else if( e.row.data.station.board_uuid !== undefined) {
        e.items = [
          {
            text: "Show Board Details", onItemClick: () => {
              this.router.navigate(['/board-detail/' + e.row.data.station.board_uuid]);
            }            
          },
        ];
      }
    }
  }

  edit(e){
    console.log(e);
  }

  onContentReady(e) {
  }

  onCellClick(e) {
    if(e.row.data.station.board_uuid == null || e.row.data.station.testid_ctl == undefined)
      return;
    let testPending = Boolean(e.row.data.station.state == "TESTS_RUNNING");
    this.loadingVisible = true;
    this.stBoard.getBoardtestDetail(e.row.data.station.board_uuid, e.row.data.station.testid_ctl, -1, 0, testPending ).subscribe( resp => {
      this.stBoardSelected = [];
      resp.test.forEach( element => {
        let t = new TestDetail(
            element.testid_ctl,
            element.status,
            element.ts,
            element.test_detail_status,
            element.result,
            element.testgroupid,
            element.testdefname,
            element.des
        )
        this.stBoardSelected.push(t);
      })
      this.loadingVisible = false;
    }, errorMessage => {
      console.log(errorMessage);
      this.loadingVisible = false;
    });
  }

  checkIcon(data, IconTypeCheck){
    return data.data.StationIcon == IconTypeCheck;
  }

}

