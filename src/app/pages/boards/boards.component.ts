import { Component, OnInit, ViewChild } from '@angular/core';
import { BoardsService } from 'src/app/shared/services';
import { I_Board_Msg, I_Board } from 'src/app/models/board.model';

import { DxDataGridComponent,
  DxDataGridModule,
  DxSelectBoxModule,
  DxCheckBoxModule } from 'devextreme-angular';
import { I_TestDetail } from 'src/app/models/test.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {
  @ViewChild(DxDataGridComponent, { static: false }) dataGrid: DxDataGridComponent;

  boards : I_Board[];
  testDetail : I_TestDetail[];

  applyFilterTypes: any;
  showFilterRow: boolean;
  showHeaderFilter: boolean;
  resizingModes: string[] = ["nextColumn", "widget"];
  columnResizingMode: string;

  constructor( private Boards : BoardsService, private router: Router ) {
    this.showFilterRow = true;
    this.showHeaderFilter = true;
    this.applyFilterTypes = [{
                  key: "auto",
                  name: "Immediately"
                },
                {
                  key: "onClick",
                  name: "On Button Click"
                }];
    this.columnResizingMode = this.resizingModes[1];
  }

  ngOnInit() {
    this.Boards.getBoardlist('10000', '0').subscribe((data: I_Board_Msg) => {
      if(data.status == "ok"){
        this.boards = data.boards;
      }
      console.log(data);
    });
  }

  currentFilter(){
    return this.applyFilterTypes[0];
  }

  clearFilter() {
    this.dataGrid.instance.clearFilter();
  }

  onCellClick(e) {
    if(e.row != undefined)
      this.Boards.getBoardAllTestDetail(e.row.data.board_uuid, -1, 0, 'testid_ctl', 'DESC').subscribe( resp => {
        this.testDetail = resp.test;
      });
  }

  onContextMenuPreparing(e){
    if (e.row != undefined && e.row.rowType == "data") {
      e.items = [
        {
          text: "Show Board Details", onItemClick: () => {
            this.router.navigate(['/board-detail/' + e.row.data.board_uuid]);
          }
        }
      ]
    }
  }  

}
