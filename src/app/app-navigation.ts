const navigation = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'Examples',
    icon: 'folder',
    items: [
      {
        text: 'Profile',
        path: '/profile'
      },
      {
        text: 'Display Data',
        path: '/display-data'
      }
    ]
  }
];

const navigationAdmin = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'User',
    path: '/profile',
    icon: 'user'
  },
  {
    text: 'Test',
    icon: 'folder',
    items: [
      {
        text: 'Test Operations',
        path: '/test'
      },
      {
        text: 'Boards',
        path: '/board'
      }
    ]
  },
  {
    text: 'RMA',
    icon: 'folder',
    items: [
      {
        text: 'RMA Admin',
        path: '/rma'
      }
    ]
  },
  {
    text: 'Administration',
    icon: 'folder',
    items: [
      {
        text: 'Model',
        path: '/model'
      },
      {
        text: 'Test Definition',
        path: '/testdef'
      },
      {
        text: 'Board Test',
        path: '/btest'
      }
    ]
  }
];

const navigation_OPER = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'User',
    path: '/profile',
    icon: 'user'
  },
  {
    text: 'Test',
    icon: 'folder',
    items: [
      {
        text: 'Test Operations',
        path: '/test'
      },
      {
        text: 'Boards',
        path: '/board'
      }
    ]
  }
];

export const navigation_roles = {
  'DEVEL'     :  navigationAdmin,
  'ADMIN'     :  navigationAdmin,
  'USER'      :  navigation,
  'CUSTOMER'  :  navigation,
  'OPER'      :  navigation_OPER,
};

const rolePathnavAccess_ADMIN = ['/home', '/profile', '/test', '/board', '/board-detail', '/rma', '/model', '/testdef', '/btest'];
const rolePathnavAccess_OPER = ['/home', '/profile', '/test', '/board', '/board-detail'];
const rolePathnavAccess_NAV = ['/home', '/profile'];


export const navPathAcess = {
  'DEVEL'     :  rolePathnavAccess_ADMIN,
  'ADMIN'     :  rolePathnavAccess_ADMIN,
  'USER'      :  rolePathnavAccess_NAV,
  'CUSTOMER'  :  rolePathnavAccess_NAV,
  'OPER'      :  rolePathnavAccess_OPER,
};