import { Component, NgModule } from '@angular/core';
import { DxChartModule } from 'devextreme-angular';
import { I_STAT_TEST_TOTAL_DAY } from 'src/app/models/stats.model';
import { StatsService } from '../../services';

@Component({
  selector: 'app-statbyday',
  templateUrl: './statbyday.component.html',
  styleUrls: ['./statbyday.component.scss']
})
export class StatbydayComponent {

  TotaltestCountByDay : I_STAT_TEST_TOTAL_DAY[] = [];

  constructor( private stService : StatsService ) { 

    stService.getStatTestTotals().subscribe( resp => {
      this.TotaltestCountByDay = resp.data;
    }, errorMessage => {
      console.log(errorMessage);
    })

  }

}

@NgModule({
  declarations: [ StatbydayComponent ],
  exports: [ StatbydayComponent ],
  imports: [ DxChartModule ]
})
export class StatbydayModule { }
