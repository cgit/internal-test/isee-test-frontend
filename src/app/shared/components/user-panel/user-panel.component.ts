import { Component, NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DxListModule } from 'devextreme-angular/ui/list';
import { DxContextMenuModule } from 'devextreme-angular/ui/context-menu';
import { AuthService } from '../../services';

@Component({
  selector: 'app-user-panel',
  templateUrl: 'user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})

export class UserPanelComponent {

  photo : any;

  @Input()
  menuItems: any;

  @Input()
  menuMode: string;

  constructor(private auth : AuthService) {
    // this.photo = 'https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/employees/07.png';
    this.photo ='';
  }

  getUserEmail(){
    return this.auth.user.email;
  }

}

@NgModule({
  imports: [
    DxListModule,
    DxContextMenuModule,
    CommonModule
  ],
  declarations: [ UserPanelComponent ],
  exports: [ UserPanelComponent ]
})
export class UserPanelModule { }
