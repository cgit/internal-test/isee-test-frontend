import { Component, NgModule, Input } from '@angular/core';
import { DxChartModule, DxCircularGaugeModule } from 'devextreme-angular';
import { Observable } from 'rxjs';
import { Station } from 'src/app/models/station.model';
import { StationService } from '../../services';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent {

  // size : 10;  
  myStations : Observable<Station[]>;
  numStations : number = 0;
  boardRunning : number = 0;

  constructor(private stService : StationService) {

    this.myStations = stService.getMyStations();
    this.myStations.subscribe((data: Station[]) => {
      if(this.boardRunning != data.length)
       this.numStations = data.length;
      let count = 0;
      data.forEach ( st => {
        if(st.heartbeat.f_timeout > 0)
          count++;
      })
      if(count != this.boardRunning)
        this.boardRunning = count;
      console.log(count);      
    });

    this.stService.getStations().subscribe( resp => {
      this.numStations = resp.stations.length;
      let count = 0;
      resp.stations.forEach( element => {
        if( element.heartbeat.f_timeout > 0)
          count++;        
      })
      if(count != this.boardRunning)
        this.boardRunning = count;
      console.log(count);
    });

  }

  customizeText(arg: any) {
    return arg.valueText + " %";
  }  
}

@NgModule({
  declarations: [ GraphComponent ],
  exports: [ GraphComponent ],
  imports: [ DxCircularGaugeModule, DxChartModule, ]
})
export class GraphModule { }