import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import { AuthService, AppInfoService, IoeventsService } from '../../services';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxCheckBoxModule } from 'devextreme-angular/ui/check-box';
import { DxTextBoxModule } from 'devextreme-angular/ui/text-box';
import { DxValidatorModule } from 'devextreme-angular/ui/validator';
import { DxValidationGroupModule } from 'devextreme-angular/ui/validation-group';
import Swal from 'sweetalert2';

import { I_Users, I_User_Login, UserLogin ,I_Login_Resp } from '../../../models/user.model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  logUser : UserLogin;
  rememberme : boolean;

  constructor(private authService: AuthService, public appInfo: AppInfoService, private router : Router, private ioEvents : IoeventsService ) {
    this.logUser = new UserLogin(null, null, null);
  }

  onLoginClick(args) {
    /* console.log(args); */
    if (!args.validationGroup.validate().isValid) {
      return;
    }
    this.authService.login(this.logUser, this.rememberme)
      .subscribe( resp => {
          this.ioEvents.sendAuth(this.authService.token);
          this.router.navigate(['/']);
      }, errorMessage => {
        if(errorMessage != "Access Denied")
          Swal.fire({
            icon: 'error',
            title: 'Ops...',
            text: errorMessage
          });
      });


    args.validationGroup.reset();
  }
}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxButtonModule,
    DxCheckBoxModule,
    DxTextBoxModule,
    DxValidatorModule,
    DxValidationGroupModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [ LoginComponent ],
  exports: [ LoginComponent ]
})
export class LoginModule { }
