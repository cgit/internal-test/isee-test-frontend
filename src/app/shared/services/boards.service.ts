import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Observable, Subscriber, Subscription, Subject, BehaviorSubject } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';

import { interval } from "rxjs/internal/observable/interval";
import { ThrowStmt } from '@angular/compiler';

import { I_TestDetail, I_TestDetail_Msg, tm_TestFile } from 'src/app/models/test.model';
import { environment } from 'src/environments/environment';

import { I_Board_Msg, tm_BoardModel_Info, tm_BoardTestResults, tm_BoardDelete_Msg } from 'src/app/models/board.model';


@Injectable({
  providedIn: 'root'
})
export class BoardsService {

  constructor(private httpClient: HttpClient) { }

  getBoardtestDetail(boarduuid, testid_ctl, limit, offset, testPending): Observable<I_TestDetail_Msg> {

    let params = new HttpParams();
    params = params.append('testid_ctl',testid_ctl);
    params = params.append('limit',limit);
    params = params.append('offset',offset);
    params = params.append('testpending',testPending);

    return this.httpClient.get<I_TestDetail_Msg>(environment.URL_apiServer + '/board/' + boarduuid + '/test',  {params: params})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  getBoardAllTestDetail(boarduuid, limit, offset, orderby, ordertype): Observable<I_TestDetail_Msg> {

    let params = new HttpParams();
    params = params.append('limit',limit);
    params = params.append('offset',offset);
    params = params.append('orderby',orderby);
    params = params.append('ordertype',ordertype);
    return this.httpClient.get<I_TestDetail_Msg>(environment.URL_apiServer + '/board/' + boarduuid + '/test',  {params: params})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }


  getBoardlist(limit, offset) : Observable<I_Board_Msg> {
    let params = new HttpParams();
    params = params.append('limit',limit);
    params = params.append('offset',offset);
    return this.httpClient.get<I_Board_Msg>(environment.URL_apiServer + '/board',  {params: params})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  getBoardDetail(b_id) : Observable<tm_BoardModel_Info>{
    return this.httpClient.get<tm_BoardModel_Info>(environment.URL_apiServer + '/board/' + b_id, {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  getBoardTestFile(b_id, testid_ctl) : Observable<tm_TestFile>{
    return this.httpClient.get<tm_TestFile>(environment.URL_apiServer + '/board/' + b_id + '/test/' + testid_ctl + '/files', {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  getBoardTestResults(b_id) : Observable<tm_BoardTestResults>{
    return this.httpClient.get<tm_BoardTestResults>(environment.URL_apiServer + '/board/' + b_id + '/test/results', {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  deleteBoard(b_id) : Observable<tm_BoardDelete_Msg>{
    return this.httpClient.delete<tm_BoardDelete_Msg>(environment.URL_apiServer + '/board/' + b_id , {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  // 192.168.2.48:8080/board/test/file/14573
  getBoardTestResultFile(fileid) : Observable<any>{
    return this.httpClient.get<any>(environment.URL_apiServer + '/board/test/file/' + fileid,  { responseType: 'blob' as 'json'} )
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error != undefined) {
      // Get client-side error
      errorMessage = error.error.error;
    }
    else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
