import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Observable, Subscriber, Subscription, Subject, BehaviorSubject } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';
import { interval } from "rxjs/internal/observable/interval";
import { ThrowStmt, R3Identifiers } from '@angular/compiler';

import { environment } from 'src/environments/environment';

import { tm_Create_RMA, tm_RMA_List, tm_RMA_Action, tm_RMA_Action_File, tm_RMA_CreateAction, tm_RMA_DeleteAction } from 'src/app/models/rma.model';

@Injectable({
  providedIn: 'root'
})
export class RmaService {

  constructor(private httpClient: HttpClient) { }

  createRMA (json_values): Observable<tm_Create_RMA> {
    return this.httpClient.post<tm_Create_RMA>(environment.URL_apiServer + '/rma',  json_values)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  getRmalist() : Observable<tm_RMA_List>{
    return this.httpClient.get<tm_RMA_List>(environment.URL_apiServer + "/rma/list")
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  getRMAActionlist(rid) : Observable<tm_RMA_Action>{
    return this.httpClient.get<tm_RMA_Action>(environment.URL_apiServer + "/rma/" + rid + "/action/list")
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  getActionList() : Observable<any> {
    return this.httpClient.get<any>(environment.URL_apiServer + "/rma/action/list")
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  deleteRMA(rid){
    return this.httpClient.delete<tm_RMA_List>(environment.URL_apiServer + "/rma/" + rid)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  modifyRMA(rid, bodyParams){
    return this.httpClient.put<tm_RMA_Action>(environment.URL_apiServer + "/rma/" + rid, bodyParams)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  modifyRMA_Action(rid, aid, bodyParams){
    return this.httpClient.put<tm_RMA_Action>(environment.URL_apiServer + "/rma/" + rid + "/action/" + aid, bodyParams)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  getRMAFileGroup(rid, aid){
    return this.httpClient.post<tm_RMA_Action_File>(environment.URL_apiServer + "/rma/" + rid + "/action/" + aid + "/file", {})
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  setRMAAction(rid, newAction, ActionText){

    let bodyParams = {
      rid : rid,
      new_action: newAction,
      action_text: ActionText
    };

    return this.httpClient.post<tm_RMA_CreateAction>(environment.URL_apiServer + "/rma/" + rid + "/action", bodyParams)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  DeleteRMAAction(rid, aid){
    return this.httpClient.delete<tm_RMA_DeleteAction>(environment.URL_apiServer + "/rma/" + rid + "/action/" + aid, {})
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  OpenRMA(rid, message){
    return this.setRMAAction(rid, 'OPEN_RMA', message);
  }

  CloseRMA(rid, message){
    return this.setRMAAction(rid, 'RMA_RESOLUTION', message);
  }

  errorHandler(error) {
    let errorMessage = error.error.error;
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
