import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders  } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { I_Users, I_User_Login, I_Login_Resp, AdminRoles, UserRoles } from '../../models/user.model'
import { Observable, Subscriber, Subscription, Subject, BehaviorSubject } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as _ from "underscore";
import { navigation_roles, navPathAcess } from '../../app-navigation';

// Temp Import
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {interval} from "rxjs/internal/observable/interval";

const Jwthelper = new JwtHelperService();

@Injectable()
export class AuthService {

  token : string = '';
  user : I_Users = null;
  id : string = '';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private router: Router, private httpClient: HttpClient) {
    this.loadfromstorage();
  }

  loadfromstorage(){
    this.id = localStorage.getItem('id');
    this.token = localStorage.getItem('token');
    this.user = JSON.parse(localStorage.getItem('user'));
    // clearUser
    if(this.token == undefined || this.token == null || Jwthelper.isTokenExpired(this.token)){
      this.clearUser();
    }
  }

  savetoStorage(id : string, token : string, user : I_Users){
    // Save in local storage
    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    // Save in Object Variables
    this.id = id;
    this.token = token;
    this.user = user;
  }

  clearUser(){
    this.user = null;
    this.token = '';
    this.id = '';
    localStorage.removeItem('user');
    localStorage.removeItem('id');
    localStorage.removeItem('token');
  }

  login(user : I_User_Login, rememberme : boolean) : Observable<I_Login_Resp> {
    if(rememberme)
      localStorage.setItem('username', user.username);
    else
      localStorage.removeItem('username');

    const myhttpOptions = {
        headers: new HttpHeaders({
          'Access-Control-Allow-Origin':'*',
          'Access-Control-Allow-Methods': "POST, GET, OPTIONS, DELETE, PUT",
          'Access-Control-Allow-Headers': "x-requested-with, Content-Type, origin, authorization, accept, client-security-token"
        })
      }

    return this.httpClient.post<I_Login_Resp>(environment.URL_apiServer + '/login', user, myhttpOptions )
        .pipe(
          map( resp => {
            /* console.log(resp); */
            if(resp.status == "ok"){
              localStorage.setItem('id', String(resp.usuario.id));
              localStorage.setItem('token', resp.token);
              localStorage.setItem('user', JSON.stringify(resp.usuario));
              this.token = resp.token;
              this.user = resp.usuario;
              this.id = String(resp.usuario.id);
            }
            return resp;
          }),
          catchError(this.errorHandler),
        );
  }

  logout(){
    this.clearUser();
    this.router.navigate(['/login']);
  }

  errorHandler(error) {
    let errorMessage = '';

    if(error.status == 0){
      errorMessage = 'Connection Error, review server connection';
    }
    else if( error.status == 401 ){
      errorMessage = error.error.error;
    }
    else if( error.status == 400){
      errorMessage = error.error.error;
    }
    return throwError(errorMessage);
  }

  get isLoggedIn() {
    return this.user != null;
  }

  getPrivilege() {    
      return this.user.rol;
  }

  getAcessPath(url){

    const v = navPathAcess[this.getPrivilege()];
    for (let key in v){
      if(v[key] == url)
        return true;
    }
    return false;
  }

  modifyMyOwnUser( userChanged : any): Observable<any> {
    let body = _.pick(userChanged, ['nombre', 'apellidos', 'email']);
    return this.httpClient.put<I_Users>(environment.URL_apiServer + '/user/' + this.id,
      body
    )
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  modifyMyOwnPassword( newPassword : String): Observable<any> {
    let body = {'pass' : newPassword };
    return this.httpClient.put<I_Users>(environment.URL_apiServer + '/user/' + this.id,
      body
    )
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  updateMyOwnUser(nombre, apellidos, email){
    this.user.nombre = nombre;
    this.user.apellidos = apellidos;
    this.user.email = email;
    localStorage.setItem('user', JSON.stringify(this.user));
  }

  IsAdmin() : Boolean {    
    for(const listPriv of AdminRoles){
      if(this.getPrivilege() == listPriv) 
      return true;
    }
    return false;    
  }

}

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(private router: Router, private authService: AuthService) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        const isLoggedIn = this.authService.isLoggedIn;
        const isLoginForm = route.routeConfig.path === 'login';

        if (isLoggedIn && isLoginForm) {
            this.router.navigate(['/']);
            return false;
        }

        if (!isLoggedIn && !isLoginForm) {
            this.router.navigate(['/login']);
        }

        if(isLoggedIn){          
          const myPath = this.authService.getAcessPath('/' + route.url[0].path);
          if(!myPath){
            this.router.navigate(['/']);
            return false
          }else
            return true;                
        }

        return isLoggedIn || isLoginForm;
    }
}
