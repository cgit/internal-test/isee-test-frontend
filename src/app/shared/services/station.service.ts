import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Observable, Subscriber, Subscription, Subject, BehaviorSubject } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';

import { interval } from "rxjs/internal/observable/interval";
import { ThrowStmt } from '@angular/compiler';
import { environment } from 'src/environments/environment';

import { Station, StationMessage, StationHostVar } from '../../models/station.model'

@Injectable()
export class StationService {

  private TimerSource;
  private st: Station[] = [];
  private usersSubject = new BehaviorSubject([]);
  private obsTimer4get : Observable<Station[]>;
  private myForceSusbs : Subscription;
  private refCount : number = 0;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) {
    this.myForceSusbs = null;
    this.TimerSource = interval(30000)
      .subscribe( res => { this.timeout(res, this)});
  }

  timeout(res, t){
    if(this.refCount > 0){
      t.obsTimer4get = t.getStations().subscribe( newdata => {
        console.log(newdata);
        if(newdata.status == "ok"){
          this.st = newdata.stations;
          this.refresh();
        }
        t.obsTimer4get.unsubscribe();
      });
    }
  }

  getStations(): Observable<StationMessage> {
    return this.httpClient.get<StationMessage>(environment.URL_apiServer + '/stations/')
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  forceUpdate(){
    if(!this.myForceSusbs){
      this.myForceSusbs = this.getStations().subscribe( resp => {
        if(resp.status == "ok"){
          this.st = resp.stations;
        }
        this.myForceSusbs.unsubscribe();
        this.myForceSusbs = null;
        this.refresh();
      });
    }
  }

  refresh(){
    this.usersSubject.next(this.st);
  }

  getMyStations(): Observable<Station[]>{
    this.refCount++;
    return this.usersSubject.asObservable();
  }

  freeMyStations(t : Observable<Station[]>){
    if(this.refCount > 0)
      this.refCount--;
    t.subscribe().unsubscribe();
  }

  getStationHostVar(station: string, key : string): Observable<StationHostVar> {
    const opts = { params: new HttpParams({fromString: "key=" + key}) };
    return this.httpClient.get<StationHostVar>(environment.URL_apiServer + '/host/' + station, opts)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  getCountStationsRunning(){
    console.log(this.st);
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
  }
}
