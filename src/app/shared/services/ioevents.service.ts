import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { retry, map, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IOMsg } from '../../models/event.model';
import { AuthService } from './auth.service'


@Injectable({
  providedIn: 'root'
})
export class IoeventsService {

  isCon : boolean;

  constructor(private socket : Socket, private auth : AuthService) {

    this.isCon = false;

    socket.on('connect', resp => {
      this.isCon = true;
      if(this.auth.isLoggedIn){
        this.sendAuth(auth.token);
      }
    });

    socket.on('disconnect', () => {
      this.isCon = false;
    })
  }

  getMessage() {
    return this.socket
        .fromEvent<any>('msg')
        .pipe(
          map(data => {
            return data;
          })
        )
  }

  sendMessage(msg: any) {
    this.socket.emit('msg', msg);
  }

  sendAuth(token : string){
    let msg = { 'token' : token };
    this.socket.emit('auth', msg, function(answer){
      console.log(answer);
    });
  }

}
