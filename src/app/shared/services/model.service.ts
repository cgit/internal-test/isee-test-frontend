import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { tm_BOARD_MODEL, tm_BOARD_MODEL_MOD_DEL } from 'src/app/models/bmodel.model';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ModelService {

  constructor(private httpClient: HttpClient) { }

  getModelTable() : Observable<tm_BOARD_MODEL> {
    return this.httpClient.get<tm_BOARD_MODEL>(environment.URL_apiServer + '/bmodel', {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )    
  }

  CreateNewModel( params ) : Observable<tm_BOARD_MODEL> {
    return this.httpClient.post<tm_BOARD_MODEL>(environment.URL_apiServer + '/bmodel', params)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  ModifyModel (id, params) : Observable<tm_BOARD_MODEL_MOD_DEL> {
    return this.httpClient.put<tm_BOARD_MODEL_MOD_DEL>(environment.URL_apiServer + '/bmodel/' + id, params)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  DeleteModel (id) : Observable<tm_BOARD_MODEL_MOD_DEL> {
    return this.httpClient.delete<tm_BOARD_MODEL_MOD_DEL>(environment.URL_apiServer + '/bmodel/' + id, {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = error.error.error;
     }     
     return throwError(errorMessage);
  }    
}
