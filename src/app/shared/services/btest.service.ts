import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { tm_BOARD_TEST, tm_BOARD_VAR, tm_I_BOARD_OP, tm_I_BOARD_TEST } from 'src/app/models/btest.model';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BtestService {

  constructor(private httpClient: HttpClient) { }

  /* Get Test Definition list */
  getBoardTestList() : Observable <tm_BOARD_TEST> {
    return this.httpClient.get<tm_BOARD_TEST>(environment.URL_apiServer + '/btest', {})
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  getBoardTestVars(id) : Observable <tm_BOARD_VAR> {
    return this.httpClient.get<tm_BOARD_VAR>(environment.URL_apiServer + '/btest/' + id + '/var', {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )  
  }

  CreateBoardTest(testgroupid, testdefid) : Observable<tm_I_BOARD_TEST> {
    let params = {};
    params['testgroupid'] = testgroupid;
    params['testdefid'] = testdefid;
    return this.httpClient.post<tm_I_BOARD_TEST>(environment.URL_apiServer + '/btest', params)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  DeleteBoardTest(testid) : Observable<tm_I_BOARD_OP> {
    return this.httpClient.delete<tm_I_BOARD_OP>(environment.URL_apiServer + '/btest/' + testid, {})
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  createNewVar(testid, varname, varvalue) : Observable<any> {
    let params = {};
    params['varname'] = varname;
    params['varvalue'] = varvalue;
    return this.httpClient.post<any>(environment.URL_apiServer + '/btest/' + testid + '/var', params)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  updateVar(testid, vid, params) : Observable<any>{
    return this.httpClient.put<any>(environment.URL_apiServer + '/btest/' + testid + '/var/' + vid, params)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  deleteVar(testid, vid) : Observable<any>{
    return this.httpClient.delete<any>(environment.URL_apiServer + '/btest/' + testid + '/var/' + vid, {})
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }
  

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = error.error.error;
     }     
     return throwError(errorMessage);
  }

}
