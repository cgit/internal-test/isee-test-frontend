import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { tm_TEST_DEFINITION, tm_TEST_DEFINITION_DELETE_MOD, tm_TEST_DEF_VAR } from 'src/app/models/tdef.model';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TestdefinitionService {

  constructor(private httpClient: HttpClient) { }
  
  /* Get Test Definition list */
  getTestDefinitions() : Observable <tm_TEST_DEFINITION> {
    return this.httpClient.get<tm_TEST_DEFINITION>(environment.URL_apiServer + '/tdef', {})
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  /* Create new definition */
  CreateTestDefinition( params ) : Observable <tm_TEST_DEFINITION> {
    let myParams = {
      t_defName : params.testdefname ,
      t_defDescription: params.des 
    }
    return this.httpClient.post<tm_TEST_DEFINITION>(environment.URL_apiServer + '/tdef', myParams )
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  /* Delete definition */
  DeleteTestDefinition ( id ) : Observable<tm_TEST_DEFINITION_DELETE_MOD> {
    return this.httpClient.delete<tm_TEST_DEFINITION_DELETE_MOD>(environment.URL_apiServer + '/tdef/' + id , {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  /* Delete definition */
  ModifyTestDefinition (id, params) : Observable<tm_TEST_DEFINITION_DELETE_MOD> {    
    return this.httpClient.put<tm_TEST_DEFINITION_DELETE_MOD>(environment.URL_apiServer + '/tdef/' + id , params)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )
  }

  /* Get Test Definition Assigned auto Creation Variables */
  getTestDefVars(id) : Observable <tm_TEST_DEF_VAR> {
    return this.httpClient.get<tm_TEST_DEF_VAR>(environment.URL_apiServer + '/tdef/' + id + '/var', {})
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  CreateTestDefinitionVar(id, params) : Observable<tm_TEST_DEF_VAR> {
    return this.httpClient.post<tm_TEST_DEF_VAR>(environment.URL_apiServer + '/tdef/' + id + '/var', params)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  ModifyTestDefinitionVar(id, nParams) : Observable<tm_TEST_DEFINITION_DELETE_MOD> {
    return this.httpClient.put<tm_TEST_DEFINITION_DELETE_MOD>(environment.URL_apiServer + '/tdef/' + id + '/var', nParams)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }

  DeleteTestDefinitionVar(id, nParams) : Observable<tm_TEST_DEFINITION_DELETE_MOD> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: {'varname' : nParams.varname}
    };
    return this.httpClient.delete<tm_TEST_DEFINITION_DELETE_MOD>(environment.URL_apiServer + '/tdef/' + id + '/var', httpOptions)
    .pipe(
      catchError(this.errorHandler),
      map( resp => resp )
    )
  }


  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = error.error.error;
     }     
     return throwError(errorMessage);
  }

}
