import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { tm_STAT_TEST_TOTAL_DAY } from 'src/app/models/stats.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private httpClient: HttpClient) { }

  getStatTestTotals() : Observable<tm_STAT_TEST_TOTAL_DAY> {
    return this.httpClient.get<tm_STAT_TEST_TOTAL_DAY>(environment.URL_apiServer + '/stats', {})
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
      )    
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
  }  

}
