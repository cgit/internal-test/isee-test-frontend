import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Observable, Subscriber, Subscription, Subject, BehaviorSubject } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';
import { interval } from "rxjs/internal/observable/interval";

import { tm_Upload_list, tm_FileDelete } from "../../models/upload.model";

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private httpClient: HttpClient) { }


  upload_file( myfile : File, f_groupid : Number) : Observable<any> {

    const formData = new FormData();
    formData.append('file', myfile);
    formData.append('f_groupid', String(f_groupid));

    return this.httpClient.post<any>(environment.URL_apiServer + '/upload/file', formData)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  getFilelist_by_groupid(f_group_id : Number) : Observable<tm_Upload_list> {
    return this.httpClient.get<tm_Upload_list>(environment.URL_apiServer + '/upload/group/' + f_group_id)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  getfileurl( data ) {
    return environment.URL_apiServer + '/upload/file/' + data['f_id'] + '/oid';
  }

  deleteFile( f_id ){
    return this.httpClient.delete<tm_FileDelete>(environment.URL_apiServer + '/upload/file/' + f_id)
      .pipe(
        catchError(this.errorHandler),
        map( resp => resp )
    )
  }

  errorHandler(error) {
    let errorMessage = error.error.error;
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
